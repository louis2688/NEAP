#!/bin/bash

# script to append major-minor-isoform info to results/02_mapping_to_evidences/trypsin_l50_m2/kim and results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm

i_end=''

for i in ../../results/02_mapping_to_evidences/trypsin_l50_m2/kim/* ; do 	#kim data set

        i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/pancreas -> pancreas
        evi="_evidence"
        i_end_plus_evi=$i_end$evi	#concat strings
        perl append_mm_info_to_mapping.pl -f $i/transcript_gene_disc_mapping.tsv -m ../../results/10_major_minor_isoform/kim/$i_end_plus_evi/major.tsv -o ../../results/02_mapping_to_evidences/trypsin_l50_m2/kim/$i_end/transcript_gene_disc_mapping_with_major.tsv

done

echo "========== Kim data set finished =========="

# for i in ../../results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/* ; do   #kim data set

#         i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/pancreas -> pancreas
#         mkdir -p ../../results/08_identify_splicing_events/wilhelm/$i_end
#         perl find_splicing_events.pl $flag -f $i/transcript_gene_disc_mapping.tsv -o ../../results/08_identify_splicing_events/kim/$i_end/$output

# done

# echo "========== Wilhelm data set finished =========="