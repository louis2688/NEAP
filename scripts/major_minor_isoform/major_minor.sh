#!/bin/bash

# script to count peptide matches on major or minor isoform for results/02_mapping_to_evidences/trypsin_l50_m2/kim and results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm
# two different strategies: -t / -g
# usage example: "bash major_minor.sh <my_output_file_name(no file ending)> -t/-g -h <proteome_file.tsv(only with -t)>" 

i_end=''
output=$1
approach=$2
proteome=$4
header=$3



for i in /media/nick/Volume/neap/project/other_data/03_coverage/Kim_Search_Results/* ; do 	#kim data set

        evi_cov="_coverage_unique.tsv"
	evi_evi='_evidence.txt'
        i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/pancreas_evidence -> pancreas_evidence
        i_end_no_evidence=${i_end%%_*}
        i_end_no_evidence_plus_evi=$i_end_no_evidence$evi_evi	#concat strings: pancreas -> pancreas_evidence.txt
        i_end_plus_evi_cov=$i_end$evi_cov	#concat strings: pancreas -> pancreas_coverage_unique.tsv
        proteome_name=${proteome##*/}           #proteome name without path
        proteome_name_no_tsv=${proteome_name%%.tsv}
        i_end_plus_approach="kim_"$output"_"$i_end_no_evidence"_"$proteome_name_no_tsv".tsv"	#concat strings: pancreas -> pancreas_$output

        mkdir -p ../../results/10_major_minor_isoform/kim/$i_end_no_evidence

        perl major_minor_isoform.pl -f $i/$i_end_plus_evi_cov -e ../../data/kim/$i_end_no_evidence_plus_evi -o ../../results/10_major_minor_isoform/kim/$i_end_no_evidence/$i_end_plus_approach $approach $header -p $proteome 

done

echo "========== Kim data set finished =========="

for i in /media/nick/Volume/neap/project/other_data/03_coverage/Wilhelm_Search_Results/* ; do 	#kim data set

	evi_cov="_coverage_unique.tsv"
	evi_evi='_evidence.txt'
        i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/pancreas -> pancreas
        i_end_no_evidence=${i_end%%_*}
        i_end_no_evidence_plus_evi=$i_end_no_evidence$evi_evi	#concat strings
        i_end_plus_evi_cov=$i_end$evi_cov	#concat strings
        proteome_name=${proteome##*/}
        proteome_name_no_tsv=${proteome_name%%.tsv}
        i_end_plus_approach="wilhelm_"$output"_"$i_end_no_evidence"_"$proteome_name_no_tsv".tsv"	#concat strings

        mkdir -p ../../results/10_major_minor_isoform/wilhelm/$i_end_no_evidence

        perl major_minor_isoform.pl -f $i/$i_end_plus_evi_cov -e ../../data/wilhelm/$i_end_no_evidence_plus_evi -o ../../results/10_major_minor_isoform/wilhelm/$i_end_no_evidence/$i_end_plus_approach $approach $header -p $proteome 

done

echo "========== Wilhelm data set finished =========="