#!/bin/bash

# script to count peptide matches on major or minor isoform for simulation
# two different strategies: -t / -g
# usage example: "bash major_minor_sim.sh <my_output_file_name(no file ending)> -t/-g -h <proteome_file.tsv(only with -t)>"

i_end=''

output=$2
approach=$3
header=$4
proteome=$5
#basefolder scripts $6


mkdir -p ./10_major_minor_isoform/simulation

for i in $1* ; do 	#kim data set

        sim="Sim_"
        i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/pancreas_evidence -> pancreas_evidence
        i_end_no_tsv=${i_end%%.tsv}
        proteome_name=${proteome##*/}           #proteome name without path
        proteome_name_no_tsv=${proteome_name%%.tsv}
        sim_plus_i_end=$sim$output"_"$i_end_no_tsv"_"$proteome_name_no_tsv".tsv"

        perl $6/scripts/major_minor_isoform/major_minor_isoform.pl -f $i -s -o 10_major_minor_isoform/simulation/$sim_plus_i_end $approach $header -p $proteome 

done

echo "========== Simulation data set finished =========="
