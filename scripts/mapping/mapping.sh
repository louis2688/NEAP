#!/bin/bash

# script to create mapping for every evidence file in /kim and /wilhelm
# TODO: mkdir evidences! -> mkdir -p
i_end=""

for i in ../../data/kim/*.txt; do 	#kim data set

	i_end=${i##*/}		#cut for example ../data/kim/pancreas_evidence.txt -> pancreas_evidence.txt
	i_end_no_evidence_dot_txt=${i_end%_*} 	#cut pancreas_evidence.txt -> pancreas

 	for j in 1 2 3; do
		if [ $j = 1 ]; then
			dummy="yes"
        #	perl peptide_mapping.pl ../results/01_in_silico_digestion_dedup/trypsin_l50_m2/transcript_disc.tsv $i ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/$i_end_no_evidence_dot_txt/transcript_disc_mapping.tsv $j
        elif [ $j = 2 ]; then
        	perl peptide_mapping.pl /media/nick/Volume/neap/project/other_data/results_new/trypsin/transcript_gene_disc.tsv $i ../../results/02_mapping_to_evidences/trypsin_l50_m2/kim/$i_end_no_evidence_dot_txt/transcript_gene_disc_mapping.tsv $j
        elif [ $j = 3 ]; then
        	dummy="yes"
        #	perl peptide_mapping.pl ../results/01_in_silico_digestion_dedup/trypsin_l50_m2/trypsin_unique.tsv $i ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/$i_end_no_evidence_dot_txt/trypsin_unique_mapping.tsv $j
        fi	
	done
done

echo "========== Kim data set finished =========="

for i in ../../data/wilhelm/*.txt; do 		#wilhelm data set

	i_end=${i##*/}		#cut for example ../data/kim/pancreas_evidence.txt -> pancreas_evidence.txt
	i_end_no_evidence_dot_txt=${i_end%_*}		#cut pancreas_evidence.txt -> pancreas

 	for j in 1 2 3; do
		if [ $j = 1 ]; then
			dummy="yes"
        #	perl peptide_mapping.pl ../results/01_in_silico_digestion_dedup/trypsin_l50_m2/transcript_disc.tsv $i ../results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/$i_end_no_evidence_dot_txt/transcript_disc_mapping.tsv $j
        elif [ $j = 2 ]; then
        	perl peptide_mapping.pl /media/nick/Volume/neap/project/other_data/results_new/trypsin/transcript_gene_disc.tsv $i ../../results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/$i_end_no_evidence_dot_txt/transcript_gene_disc_mapping.tsv $j
        elif [ $j = 3 ]; then
        	dummy="yes"
        #	perl peptide_mapping.pl ../results/01_in_silico_digestion_dedup/trypsin_l50_m2/trypsin_unique.tsv $i ../results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/$i_end_no_evidence_dot_txt/trypsin_unique_mapping.tsv $j
        fi
	done
done
output
echo "========== Wilhelm data set finished =========="