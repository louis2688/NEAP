#!/usr/bin/perl
#use warnings;

# This script searches in silico digested peptides in *_evidence.txt files!
# Output file: {tissue}_peptides_mapping.txt
# @author: nick

#TODO: Header format as input parameter

use Data::Dumper;
use Getopt::Long qw(GetOptions);


$in_silico = $ARGV[0] or die(
	"\nNo in-silico peptide file given!\n\nUsage:\nperl peptide_mapping.pl <in_silico_peptides> <{tissue}_evidence> <output_file> <in_silico_peptides-format>\n\n".
	"E.g.:\nperl peptide_mapping.pl in_silico_peptides.tsv pancreas_evidence.txt output_file 1\n");

$evidence = $ARGV[1] or die("\nNo evidence file given!\n\nUsage:\nperl peptide_mapping.pl <in_silico_peptides> <{tissue}_evidence> <output_file> <in_silico_peptides-format>\n\n".
	"E.g.:\nperl peptide_mapping.pl in_silico_peptides.tsv pancreas_evidence.txt output_file 1\n");

$output_file = $ARGV[2] or die ("\nNo output file given!\n\nUsage:\tperl peptide_mapping.pl <in_silico_peptides.tsv> <{tissue}_evidence.txt> <output_file> <in_silico_peptides-format>\n\n".
	"E.g.: perl peptide_mapping.pl in_silico_peptides.tsv pancreas_evidence.txt output_file 1\n");

$format = "".$ARGV[3] or die("\nNo in-silico characteristic given!\n\nModes:\n<1> = peptides that discriminate between two transcripts and do not reappear in other genes\n<2> = peptides that discriminate between two transcripts, but can appear in other genes\n<3> = all unique peptides\n");

# print "Specify output file (path) for $evidence!\n";		# different output-file parameter approach (via STDIN)
# $output_file=<STDIN>;
# chomp $output_file;
print "\nSaving output to $output_file\n";

open(IN_S,	'<', $in_silico) or die("\nNo in-silico peptide file found! Use relative path.\n"); #in silico file open
open(IN_E,	'<', $evidence) or die("\nNo evidence file found! Use relative path.\n"); #evidence file open
open(OUT,	'>', $output_file);

my %all_in_silico=();
my %all_in_evidence=();

while(<IN_S>){	#line by line read of silico file
	
	$count_is_entries++;
	
	if ($format =~ "1" || $format =~ "2") {
		
		chomp($_);
		@split=split("\t",$_);
#		@gene_name_unprocessed=split(":",$split[0]);	# splits: e.g. "gene:ENSG00000211804.3" -> "ENSG00000211804.3"
		my $gene_name=$split[0];
		#	print "\nSplit[1]: $split[1]";
#		@transcripts_unprocessed_all=split(/\|/,$split[1]);	# splits: e.g. "transcript:ENST00000390452.2|transcript:ENST00000390452.3" -> "transcript:ENST00000390452.2"
		#	print "\nTranscripts_unp_all: ";
		#	print Dumper @transcripts_unprocessed_all;
		# foreach my $t (@transcripts_unprocessed_all) {
		# 	my @transcripts_unprocessed_one = split(":",$t);
		# 	push (@transcripts, $transcripts_unprocessed_one[1]);
		# }
		#	print "\nTranscripts: ";
		#	print Dumper @transcripts;
#		my @transcripts_sorted = sort @transcripts;		#sorts transcripts, so the order doesn't matter when putting in hash "%all_in_silico"
#		my $transcripts_as_string = join(";",@transcripts_sorted);
#		undef @transcripts;
		
		$t1=$split[1];	#transcript 1
		$t2=$split[2];	#transcript 2
		$in_silico_peptide = $split[3];		#peptide

		$all_in_silico{$in_silico_peptide}{$gene_name}{$t1}{$t2}=1;
	}

	elsif ($format =~ "3") {
		chomp($_);
		$all_in_silico{$_}=1;
	}
}
close IN_S;

print "\n1. In-silico file read.\n";

while(<IN_E>){	#line by line read of evidence file
	
	$count_ev_entries++;
	if ($count_ev_entries==1){
		chomp($_);
		my @split=split("\t",$_);
		$header="$split[1]\t$split[6]\t$split[7]\t$split[8]\t$split[9]\t$split[11]\t$split[18]\t$split[47]\t$split[48]";		#selected evidence info
	}
	elsif ($count_ev_entries>1) {	#skip header

		chomp($_);
		my @split=split("\t",$_);
		my $peptide=$split[0];
		my $info = "$split[1]\t$split[6]\t$split[7]\t$split[8]\t$split[9]\t$split[11]\t$split[18]\t$split[47]\t$split[48]";		#selected evidence info
		#shift(@split);		# shift array to previous index: $split[1] is $split[0] now
	#	$info=join("\t",@split);
		$all_in_evidence{$peptide}{$info}=1;
	}
}
close IN_E;
print "2. Evidence file read.\n";

print "3. Mapping matching peptides.\n";

if ($format =~ "1"|| $format =~ "2") {		#transcript-specific & proteome unique
	print OUT "Peptide\tGene\tTranscript1\tTranscript2\t$header\n";		#insert header in file
	foreach my $peptide (keys %all_in_silico){		#find matching peptides and write to specified output file
		if(exists($all_in_evidence{$peptide})){
			foreach my $gene_name (keys %{$all_in_silico{$peptide}}){
				foreach my $transcript1 (keys %{$all_in_silico{$peptide}{$gene_name}}){
					foreach my $transcript2 (keys %{$all_in_silico{$peptide}{$gene_name}{$transcript1}}){
						foreach my $evidence_info (keys %{$all_in_evidence{$peptide}}){
							print OUT "$peptide\t$gene_name\t$transcript1\t$transcript2\t$evidence_info\n";
						}
					}
				}
			}
		}
	}
}
elsif($format =~ "3"){		#proteome unique
	print OUT "Peptide\t$header\n";		#insert header in file
	foreach my $peptide (keys %all_in_silico){		#find matching peptides and write to specified output file
		if(exists($all_in_evidence{$peptide})){
			foreach my $evidence_info (keys %{$all_in_evidence{$peptide}}){
				print OUT "$peptide\t$evidence_info\n";
			}
		}
	}
}
print "\nFinished!\n";