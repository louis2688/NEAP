setwd("/home/nick/neap/git/NEAP")
library("ggplot2", lib.loc="~/R/x86_64-pc-linux-gnu-library/3.2")
library(dplyr)

##### Read evidence files

folder_wilhelm <- "/home/nick/neap/git/NEAP/data/wilhelm/"   # path to folder that holds multiple evidence files
file_list_wilhelm <- list.files(path=folder_wilhelm, pattern="*.txt") # create list of all evidence files in folder

folder_kim <- "/home/nick/neap/git/NEAP/data/kim/"   # path to folder that holds multiple evidence files
file_list_kim <- list.files(path=folder_kim, pattern="*.txt") # create list of all evidence files in folder

# read in each evidence file in file_list and rbind them into a data frame called data 
data_kim_ev <- 
  do.call("rbind", 
          lapply(file_list_kim, 
                 function(x) 
                   read.csv(paste(folder_kim, x, sep=''), 
                            sep = "\t",header=T,fill=T, row.names = NULL)))

data_kim_ev<-data_kim_ev[,c(1,2,7,8,9,10,12,19,48,49)]  #keep columns of importance
gc()

data_wilhelm_ev <- 
  do.call("rbind", 
          lapply(file_list_wilhelm, 
                 function(x) 
                   read.csv(paste(folder_wilhelm, x, sep=''), 
                            sep = "\t",header=T,fill=T, row.names = NULL)))

data_wilhelm_ev<-data_wilhelm_ev[,c(1,2,7,8,9,10,12,19,48,49)]  #keep columns of importance
gc()

### Evidence statistics
npep_kim_ev<-length(data_kim_ev$Sequence)
npep_unique_kim_ev<-length(unique(data_kim_ev$Sequence))
pep_avg_length_kim_ev<-mean(data_kim_ev$Length)
pep_avg_mz_kim_ev<-mean(data_kim_ev$m.z)
pep_avg_score_kim_ev<-mean(data_kim_ev$Score)
pep_avg_score_delta_kim_ev<-mean(data_kim_ev$Delta.score)
kim_ev<-rbind(npep_kim_ev,
      npep_unique_kim_ev,
      pep_avg_length_kim_ev,
      pep_avg_mz_kim_ev,
      pep_avg_score_kim_ev,
      pep_avg_score_delta_kim_ev)
colnames(kim_ev)<-"Kim Evidence"

npep_wilhelm_ev<-length(data_wilhelm_ev$Sequence)
npep_unique_wilhelm_ev<-length(unique(data_wilhelm_ev$Sequence))
pep_avg_length_wilhelm_ev<-mean(data_wilhelm_ev$Length)
pep_avg_mz_wilhelm_ev<-mean(data_wilhelm_ev$m.z)
pep_avg_score_wilhelm_ev<-mean(data_wilhelm_ev$Score)
pep_avg_score_delta_wilhelm_ev<-mean(data_wilhelm_ev$Delta.score)
wilhelm_ev<-rbind(npep_wilhelm_ev,
                  npep_unique_wilhelm_ev,
                  pep_avg_length_wilhelm_ev,
                  pep_avg_mz_wilhelm_ev,
                  pep_avg_score_wilhelm_ev,
                  pep_avg_score_delta_wilhelm_ev)
colnames(wilhelm_ev)<-"Wilhelm Evidence"

#remove(data_kim_ev,data_wilhelm_ev,file_list_kim,file_list_wilhelm,folder_kim,folder_wilhelm)
gc()

##### Read mapping files

file.paths_wilhelm <- Sys.glob ('/home/nick/neap/git/NEAP/results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/*/*gene_disc_mapping.tsv')
file.paths_kim <- Sys.glob ('/home/nick/neap/git/NEAP/results/02_mapping_to_evidences/trypsin_l50_m2/kim/*/*gene_disc_mapping.tsv')

data_kim_map <- 
  do.call("rbind", 
          lapply(file.paths_kim, 
                 function(x) 
                   read.csv(x, sep = "\t",header=T,fill=T, row.names = NULL)))


npep_kim_map<-length(data_kim_map$Peptide)
npep_unique_kim_map<-length(unique(data_kim_map$Peptide))
pep_avg_length_kim_map<-mean(data_kim_map$Length)
pep_avg_mz_kim_map<-mean(data_kim_map$m.z)
pep_avg_score_kim_map<-mean(data_kim_map$Score)
pep_avg_score_delta_kim_map<-mean(data_kim_map$Delta.score)
kim_map<-rbind(npep_kim_map,
               npep_unique_kim_map,
               pep_avg_length_kim_map,
               pep_avg_mz_kim_map,
               pep_avg_score_kim_map,
               pep_avg_score_delta_kim_map)
colnames(kim_map)<-"Kim Mapping"

#remove(data_kim_map,file.paths_kim)
gc()

data_wilhelm_map <- 
  do.call("rbind", 
          lapply(file.paths_wilhelm, 
                 function(x) 
                   read.csv(x, sep = "\t",header=T,fill=T, row.names = NULL)))

npep_wilhelm_map<-length(data_wilhelm_map$Peptide)
npep_unique_wilhelm_map<-length(unique(data_wilhelm_map$Peptide))
pep_avg_length_wilhelm_map<-mean(data_wilhelm_map$Length)
pep_avg_mz_wilhelm_map<-mean(data_wilhelm_map$m.z)
pep_avg_score_wilhelm_map<-mean(data_wilhelm_map$Score)
pep_avg_score_delta_wilhelm_map<-mean(data_wilhelm_map$Delta.score)
wilhelm_map<-rbind(npep_wilhelm_map,
                   npep_unique_wilhelm_map,
                   pep_avg_length_wilhelm_map,
                   pep_avg_mz_wilhelm_map,
                   pep_avg_score_wilhelm_map,
                   pep_avg_score_delta_wilhelm_map)
colnames(wilhelm_map)<-"Wilhelm Mapping"

#remove(data_wilhelm_map,file.paths_wilhelm)
gc()


### make set_diff/anti_join of evidence & mapped peptides

kim_ev_map_diff<-anti_join(data_kim_ev,data_kim_map, by=c("Sequence"="Peptide"))
wilhelm_ev_map_diff<-anti_join(data_wilhelm_ev,data_wilhelm_map, by=c("Sequence"="Peptide"))

npep_kim_diff<-length(kim_ev_map_diff$Sequence)
npep_unique_kim_diff<-length(unique(kim_ev_map_diff$Sequence))
pep_avg_length_kim_diff<-mean(kim_ev_map_diff$Length)
pep_avg_mz_kim_diff<-mean(kim_ev_map_diff$m.z)
pep_avg_score_kim_diff<-mean(kim_ev_map_diff$Score)
pep_avg_score_delta_kim_diff<-mean(kim_ev_map_diff$Delta.score)
kim_diff<-rbind(npep_kim_diff,
                npep_unique_kim_diff,
                pep_avg_length_kim_diff,
                pep_avg_mz_kim_diff,
                pep_avg_score_kim_diff,
                pep_avg_score_delta_kim_diff)
colnames(kim_diff)<-"Kim Difference"

npep_wilhelm_diff<-length(wilhelm_ev_map_diff$Sequence)
npep_unique_wilhelm_diff<-length(unique(wilhelm_ev_map_diff$Sequence))
pep_avg_length_wilhelm_diff<-mean(wilhelm_ev_map_diff$Length)
pep_avg_mz_wilhelm_diff<-mean(wilhelm_ev_map_diff$m.z)
pep_avg_score_wilhelm_diff<-mean(wilhelm_ev_map_diff$Score)
pep_avg_score_delta_wilhelm_diff<-mean(wilhelm_ev_map_diff$Delta.score)
wilhelm_diff<-rbind(npep_wilhelm_diff,
                    npep_unique_wilhelm_diff,
                    pep_avg_length_wilhelm_diff,
                    pep_avg_mz_wilhelm_diff,
                    pep_avg_score_wilhelm_diff,
                    pep_avg_score_delta_wilhelm_diff)
colnames(wilhelm_diff)<-"Wilhelm Difference"


#### Combine statistics and write.csv
kim<-cbind(kim_ev,kim_map,kim_diff)
kim<-t(kim)
kim<-as.data.frame(kim)
colnames(kim)<-c("Peptides","Unique Peptides","Avg. Peptide Length","Avg. m/z","Avg. Score","Avg. Delta Score")
write.csv(kim,file="./results/05_mapping_statistics/kim_all.csv")

wilhelm<-cbind(wilhelm_ev,wilhelm_map,wilhelm_diff)
wilhelm<-t(wilhelm)
wilhelm<-as.data.frame(wilhelm)
colnames(wilhelm)<-c("Peptides","Unique Peptides","Avg. Peptide Length","Avg. m/z","Avg. Score","Avg. Delta Score")
write.csv(wilhelm,file="./results/05_mapping_statistics/wilhelm_all.csv")


#### Basic statistics of mapping files

### Kim:
#npep_kim_map<-length(data_kim_map$Peptide)
#npep_unique_kim_map<-length(unique(data_kim_map$Peptide))
pep_matched_percent_kim_map<-(npep_unique_kim_map/npep_unique_kim_ev)
ngenes_unique_kim_map<-length(unique(data_kim_map$Gene))
mean_genes_kim_map<-mean(table(data_kim_map$Gene))
df_genes_per_peptide_kim_map<-data_kim_map %>% group_by(Peptide,Gene) %>% tally()
mean_genes_per_peptide_kim_map<-mean(df_genes_per_peptide_kim_map$n)
#remove(df)
#gc()
df_coverage_per_peptide_g_t1_t2<-data_kim_map %>% group_by(Peptide,Transcript1,Transcript2) %>% tally()
mean_coverage_per_peptide_g_t1_t2<-mean(df_coverage_per_peptide_g_t1_t2$n)

kim_map_basic<-rbind(npep_kim_map,
  npep_unique_kim_map,
  pep_matched_percent_kim_map,
  ngenes_unique_kim_map,
  mean_genes_kim_map,
  mean_genes_per_peptide_kim_map,
  mean_coverage_per_peptide_g_t1_t2)
colnames(kim_map_basic)<-"Kim Mapping Basic"

### Wilhelm:
#npep_wilhelm_map<-length(data_wilhelm_map$Peptide)
#npep_unique_wilhelm_map<-length(unique(data_wilhelm_map$Peptide))
pep_matched_percent_wilhelm_map<-(npep_unique_wilhelm_map/npep_unique_wilhelm_ev)
ngenes_unique_wilhelm_map<-length(unique(data_wilhelm_map$Gene))
mean_genes_wilhelm_map<-mean(table(data_wilhelm_map$Gene))
df_genes_per_peptide_wilhelm_map<-data_wilhelm_map %>% group_by(Peptide,Gene) %>% tally()
mean_genes_per_peptide_wilhelm_map<-mean(df_genes_per_peptide_wilhelm_map$n)
#remove(df)
#gc()
df_coverage_per_peptide_g_t1_t2<-data_wilhelm_map %>% group_by(Peptide,Transcript1,Transcript2) %>% tally()
mean_coverage_per_peptide_g_t1_t2<-mean(df_coverage_per_peptide_g_t1_t2$n)

wilhelm_map_basic<-rbind(npep_wilhelm_map,
                     npep_unique_wilhelm_map,
                     pep_matched_percent_wilhelm_map,
                     ngenes_unique_wilhelm_map,
                     mean_genes_wilhelm_map,
                     mean_genes_per_peptide_wilhelm_map,
                     mean_coverage_per_peptide_g_t1_t2)
colnames(wilhelm_map_basic)<-"Wilhelm Mapping Basic"


combined_map_basic<-cbind(kim_map_basic,wilhelm_map_basic)
combined_map_basic<-t(combined_map_basic)
combined_map_basic<-as.data.frame(combined_map_basic)
colnames(combined_map_basic)<-c("Peptides","Unique Peptides","% mapped to Evidence","Unique Genes","Avg. # of Genes","Avg. # of Genes per Peptide","Avg. Coverage per Peptide T1 T2")
write.csv(combined_map_basic,file="./results/05_mapping_statistics/basic_mapping_statistics.csv")


