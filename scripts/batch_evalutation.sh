mkdir evaluation

#fixed: proteome=uniprot, program=MaxQuant, approach=longest, comparison=equal; variable: lab
filename="wilhelm_kim_longest_equal_uniprot_MQ"
python3 $1/scripts/splice_evaluation/loop_files.py results_uniprot_2015_final/10_major_minor_isoform/wilhelm results_uniprot_2015_final/10_major_minor_isoform/kim "longest" "longest" "equal" "evaluation/$filename" "$filename\_stats.tsv" "$filename\_splice.tsv" "Wilhelm" "Kim" "$filename" $1

#fixed: proteome=uniprot, program=MaxQuant, approach=greedy, comparison=equal; variable: lab
filename="wilhelm_kim_same_tissue_greedy_MQ_uniprot"
python3 $1/scripts/splice_evaluation/loop_files.py results_uniprot_2015_final/10_major_minor_isoform/wilhelm results_uniprot_2015_final/10_major_minor_isoform/kim "greedy" "greedy" "equal" "evaluation/$filename" "$filename\_stats.tsv" "$filename\_splice.tsv" "Wilhelm" "Kim" "$filename" $1

#fixed: proteome=uniprot, program=MaxQuant, lab=Kim, comparison=equal; variable: approach
filename="kim_kim_equal_longest_greedy_MQ_uniprot"
python3 $1/scripts/splice_evaluation/loop_files.py results_uniprot_2015_final/10_major_minor_isoform/kim results_uniprot_2015_final/10_major_minor_isoform/kim "longest" "greedy" "equal" "evaluation/$filename" "$filename\_stats.tsv" "$filename\_splice.tsv" "Kim_longest" "Kim_greedy" "$filename" $1

#all tissus in kim vs all tissues in Kim
filename="kim_kim_same_tissue_longest_MQ_uniprot_all"
python3 $1/scripts/splice_evaluation/loop_files.py results_uniprot_2015_final/10_major_minor_isoform/kim results_uniprot_2015_final/10_major_minor_isoform/kim "longest" "longest" "all" "evaluation/$filename" "$filename\_stats.tsv" "$filename\_splice.tsv" "Kim" "Kim" "$filename" $1

filename="kim_kim_same_tissue_greedy_MQ_uniprot_all"
python3 $1/scripts/splice_evaluation/loop_files.py results_uniprot_2015_final/10_major_minor_isoform/kim results_uniprot_2015_final/10_major_minor_isoform/kim "greedy" "greedy" "all" "evaluation/$filename" "$filename\_stats.tsv" "$filename\_splice.tsv" "Kim_greedy" "Kim_greedy" "$filename" $1

#Evaluate Uniprot vs Ensemble
#fixed:  program=MaxQuant, lab=Kim, comparison=equal, approach=longest; variable=proteome
filename="kim_ensemble_kim_uniprot_equal_MQ"
foldername="evaluation/kim_ensemble_kim_uniprot_equal_MQ"
mkdir $foldername
mkdir $foldername/scores

#longest rerun
Rscript $1/scripts/splice_evaluation/batch_version.R -e results_ensembl_final/10_major_minor_isoform/rerun/rerun/rerun_pancreas_evidence_longest-isoform_.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/pancreas/Kim_Search_Results_longest-isoform_pancreas_uniprot2015.deduplicated.tsv -s $foldername/scores/scores_rerun_pancreas_evidence_longest-isoform_.tsv -t $foldername/scores/scores_Kim_Search_Results_longest-isoform_pancreas_uniprot2015.deduplicated.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

#greedy rerun
Rscript $1/scripts/splice_evaluation/batch_version.R -e results_ensembl_final/10_major_minor_isoform/rerun/rerun/rerun_pancreas_evidence_greedy_.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/pancreas/Kim_Search_Results_greedy_pancreas_.tsv -s $foldername/scores/scores_rerun_pancreas_evidence_greedy_.tsv -t $foldername/scores/scores_Kim_Search_Results_greedy_pancreas_.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

#longest brP
Rscript $1/scripts/splice_evaluation/batch_version.R -e results_ensembl_final/10_major_minor_isoform/rerun/bRP/rerun_pancreas_bRP_evidence_longest-isoform_.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/pancreas/Kim_Search_Results_longest-isoform_pancreas_uniprot2015.deduplicated.tsv -s $foldername/scores/scores_rerun_pancreas_bRP_evidence_longest-isoform_.tsv -t $foldername/scores/scores_Kim_Search_Results_longest-isoform_pancreas_uniprot2015.deduplicated.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

#greedy brP
Rscript $1/scripts/splice_evaluation/batch_version.R -e results_ensembl_final/10_major_minor_isoform/rerun/bRP/rerun_pancreas_bRP_evidence_greedy_.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/pancreas/Kim_Search_Results_greedy_pancreas_.tsv -s $foldername/scores/scores_rerun_pancreas_bRP_evidence_greedy_.tsv -t $foldername/scores/scores_Kim_Search_Results_greedy_pancreas_.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

Rscript $1/scripts/splice_evaluation/batch_visualizer.R -f "Kim_Ensemble" -s "Kim_UniProtKB" -i  $foldername/$filename\_stats.tsv -t $foldername/$filename\_splice.tsv -o $foldername -n  "$filename"
#
filename="MQ_vs_Mascot_longest"
foldername="evaluation/MQ_vs_Mascot_longest"
mkdir $foldername
mkdir $foldername/scores

Rscript $1/scripts/splice_evaluation/batch_version.R -e results_uniprot_Mascot/10_major_minor_isoform/mascot/Adult_Liver_bRP_Velos_10/Adult_Liver_bRP_Velos_10_longest-isoform_uniprot2015.deduplicated.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/liver/Kim_Search_Results_longest-isoform_liver_uniprot2015.deduplicated.tsv -s $foldername/scores/scores_Adult_Liver_bRP_Velos_10_longest-isoform_uniprot2015.deduplicated.tsv -t $foldername/scores/scores_Kim_Search_Results_longest-isoform_liver_uniprot2015.deduplicated.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

Rscript $1/scripts/splice_evaluation/batch_version.R -e results_uniprot_Mascot/10_major_minor_isoform/mascot/Adult_Kidney_bRP_Velos_8/Adult_Kidney_bRP_Velos_8_longest-isoform_uniprot2015.deduplicated.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/kidney/Kim_Search_Results_longest-isoform_kidney_uniprot2015.deduplicated.tsv -s $foldername/scores/scores_Adult_Kidney_bRP_Velos_10_longest-isoform_uniprot2015.deduplicated.tsv -t $foldername/scores/scores_Kim_Search_Results_longest-isoform_kidney_uniprot2015.deduplicated.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

Rscript $1/scripts/splice_evaluation/batch_version.R -e results_uniprot_Mascot/10_major_minor_isoform/mascot/Adult_Pancreas_Gel_Elite_60/Adult_Pancreas_Gel_Elite_60_longest-isoform_uniprot2015.deduplicated.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/pancreas/Kim_Search_Results_longest-isoform_pancreas_uniprot2015.deduplicated.tsv -s $foldername/scores/scores_Adult_Pancreas_Gel_Elite_60_longest-isoform_uniprot2015.deduplicated.tsv -t $foldername/scores/scores_Kim_Search_Results_longest-isoform_pancreas_uniprot2015.deduplicated.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

Rscript $1/scripts/splice_evaluation/batch_visualizer.R -f "Mascot_longest" -s "Kim_MaxQuant_longest" -i  $foldername/$filename\_stats.tsv -t $foldername/$filename\_splice.tsv -o $foldername -n  "$filename"

filename="MQ_vs_Mascot_greedy"
foldername="evaluation/MQ_vs_Mascot_greedy"
mkdir $foldername
mkdir $foldername/scores

Rscript $1/scripts/splice_evaluation/batch_version.R -e results_uniprot_Mascot/10_major_minor_isoform/mascot/Adult_Liver_bRP_Velos_10/Adult_Liver_bRP_Velos_10_greedy_.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/liver/Kim_Search_Results_greedy_liver_.tsv -s $foldername/scores/scores_Adult_Liver_bRP_Velos_10_greedy_.tsv -t $foldername/scores/scores_Kim_Search_Results_greedy_liver_.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

Rscript $1/scripts/splice_evaluation/batch_version.R -e results_uniprot_Mascot/10_major_minor_isoform/mascot/Adult_Kidney_bRP_Velos_8/Adult_Kidney_bRP_Velos_8_greedy_.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/kidney/Kim_Search_Results_greedy_kidney_.tsv -s $foldername/scores/scores_Adult_Kidney_bRP_Velos_10_greedy_.tsv -t $foldername/scores/scores_Kim_Search_Results_greedy_kidney_.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

Rscript $1/scripts/splice_evaluation/batch_version.R -e results_uniprot_Mascot/10_major_minor_isoform/mascot/Adult_Pancreas_Gel_Elite_60/Adult_Pancreas_Gel_Elite_60_greedy_.tsv -f results_uniprot_2015_final/10_major_minor_isoform/kim/pancreas/Kim_Search_Results_greedy_pancreas_.tsv -s $foldername/scores/scores_Adult_Pancreas_Gel_Elite_60_greedy_.tsv -t $foldername/scores/scores_Kim_Search_Results_greedy_pancreas_.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename

Rscript $1/scripts/splice_evaluation/batch_visualizer.R -f "Mascot_greedy" -s "Kim_MaxQuant_greedy" -i  $foldername/$filename\_stats.tsv -t $foldername/$filename\_splice.tsv -o $foldername -n  "$filename"

#only to get minor isoform fraction
filename="Lamond_longest_vs_greedy"
foldername="evaluation/Lamond"
mkdir $foldername
mkdir $foldername/scores

Rscript $1/scripts/splice_evaluation/batch_version.R -e  results_uniprot_2015_final/10_major_minor_isoform/lamond/evidence/Lamond_Leukemia_Methyl_Acetyl_longest-isoform_uniprot2015.deduplicated.tsv -f results_uniprot_2015_final/10_major_minor_isoform/lamond/evidence/Lamond_Leukemia_Methyl_Acetyl_greedy_.tsv -s $foldername/scores/scores_Adult_Liver_bRP_Velos_10_greedy_.tsv -t $foldername/scores/scores_Kim_Search_Results_greedy_liver_.tsv -o $foldername/$filename\_stats.tsv -c $foldername/$filename\_splice.tsv -b $1 -m $1/data/id_mapping/ENSG_to_geneID.txt -n $1/data/id_mapping/mapping_to_gene.tsv -k $foldername/$filename


foldername="evaluation/all"
mkdir $foldername
filename="general_summary"
cat evaluation/*/*stats.tsv | sort | uniq > $foldername/concatenated_stats.tsv
sort -k2 $foldername/concatenated_stats.tsv > $foldername/concatenated_stats_sorted.tsv
cat evaluation/*/*splice.tsv | sort | uniq > $foldername/concatenated_splice.tsv
Rscript $1/scripts/splice_evaluation/batch_visualizer.R -f "All" -s "All" -i  $foldername/concatenated_stats_sorted.tsv -t $foldername/concatenated_splice.tsv -o $foldername -n  "$filename"
