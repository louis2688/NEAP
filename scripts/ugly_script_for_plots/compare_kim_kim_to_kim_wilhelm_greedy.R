require(data.table)
require(ggplot2)
kimkim<-fread("/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/evaluation/kim_kim_same_tissue_greedy_MQ_uniprot_all/kim_kim_same_tissue_greedy_MQ_uniprot_all_splice.tsv", sep="\t")
kim_wilhelm<-fread("/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/evaluation/wilhelm_kim_same_tissue_greedy_MQ_uniprot/wilhelm_kim_same_tissue_greedy_MQ_uniprot_splice.tsv", sep="\t")
mascot_maxQuant<-fread("/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/evaluation/MQ_vs_Mascot_greedy/MQ_vs_Mascot_greedy_splice.tsv", sep="\t")
ensemble_uniprot<-fread("/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/evaluation/kim_ensemble_kim_uniprot_equal_MQ/kim_ensemble_kim_uniprot_equal_MQ_splice.tsv", sep="\t")
kim_longest_greedy<-fread("/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/evaluation/kim_kim_equal_longest_greedy_MQ_uniprot/kim_kim_equal_longest_greedy_MQ_uniprot_splice.tsv", sep="\t")

kimkim<-unique(kimkim)
kim_wilhelm<-unique(kim_wilhelm)
mascot_maxQuant<-unique(mascot_maxQuant)
ensemble_uniprot<-unique(ensemble_uniprot)[2,]
kim_longest_greedy<-unique(kim_longest_greedy)
kimkim$data1<-"2"
kim_wilhelm$data1<-"1"
ensemble_uniprot$data1<-"4"
mascot_maxQuant$data1<-"3"
kim_longest_greedy$data1<-"5"
all<-rbind(kimkim, kim_wilhelm, ensemble_uniprot, mascot_maxQuant, kim_longest_greedy)

colnames(all)<-c("Dataset1", "Dataset2", "Proteins_1", "Proteins_2", "Intersect", "Same_Gene", "Same_Gene_and_Isoform", "Total", "Data")
all<-all[-which(Total==0),]
ggplot(all, aes(x=Data,y=Total))+geom_boxplot()+scale_x_discrete(labels=c("K_W_tw", "K_all_all", "Mascot_MQ", "UniProt_Enseml", "K_long_gre"))+
  theme(axis.text=element_text(size=10))+ggtitle("Size of overlaps of alternatively spliced genes between two tissues")+
  xlab("Comparison")+ylab("Size of overlap of alternatively spliced genes")

