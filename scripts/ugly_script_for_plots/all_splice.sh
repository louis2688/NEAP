cat */10_major_minor_isoform/(kim|wilhelm)/*/*longest*.tsv > all_tissues_splice_no_lamond_longest.tsv
cat */10_major_minor_isoform/(kim|wilhelm)/*/*greedy*.tsv > all_tissues_splice_no_lamond_greedy.tsv
cat */10_major_minor_isoform/*/*/*greedy*.tsv > all_tissues_splice_greedy.tsv
cat */10_major_minor_isoform/*/*/*longest*.tsv > all_tissues_splice_longest.tsv
