from Bio import SeqIO
import ahocorasick
import os.path
import argparse
parser = argparse.ArgumentParser(description='In silico digestion')
parser.add_argument('inputfile', metavar="Input", type=str,
                                        help='In silico digested file')
parser.add_argument('outdir', metavar="Outdir", type=str,
                                        help='Output directory')
parser.add_argument('min_pep', metavar="Min_pep_length", type=int,
                                        help='minimal peptide length')
parser.add_argument('max_pep', metavar="Max_pep_length", type=int,
                                        help='maximal peptide length')


args = parser.parse_args()
# Step 1: create a dictionary with genes as keys and another dictionary as value:
# which has as value the full peptide list of the insilico digested proteome
# {gene:{transcript:[peptides]}}
peptides = {}
with open(args.inputfile) as handle:
    #next(handle)
    i=0
    for line in handle:
        l=line.rstrip().split("\t")
        gene=l[0].replace("gene:", "")
        transcript=l[1].replace("transcript:", "")
        #print(transcript)
        if gene not in peptides.keys():
            peptides[gene]={transcript:set(l[2].split("|"))}
            i=i+1
        else:
            peptides[gene][transcript]=set(l[2].split("|"))
            i=i+1
print(len(peptides.keys()))
#print(peptides)
#print(i)

print("Step2")
# Step 2: create a search tree with the peptides as keys and a dictionary
# whith the genes as keys and a list of transcripts as values:
# {peptide: {gene:[transcripts]}}
A = ahocorasick.Automaton()
ppg={}
with open(args.inputfile) as handle:
    for line in handle:
        l=line.rstrip().split("\t")
        for pep in l[2].split("|"):
            if pep not in A:
                A.add_word(pep, {l[0]:set([l[1]])})
            else:
                if l[0] not in A.get(pep).keys():
                    A.get(pep)[l[0]]=set([l[1]])
                else:
                    A.get(pep)[l[0]].add(l[1])
        if l[0] not in ppg.keys():
            ppg[l[0]]=set([l[1]])
        else:
            ppg[l[0]].add(l[1])
A.make_automaton()
#print(list(A.get("MPLQLLLLLILLGPGNSLQLWDTWADEAEK")))

print("Step3")
# Step 3: make a list of peptides which discriminate via difference of the sets
# Approach iterate over the all possible combination of transcripts for each gene
# and compute the differenced of the sets of peptides (symetric difference), here
# split into two differences: t\s and s\t with t and s the sets of peptides
res=[]
for gene in peptides.keys():
    for t in range(0, len(peptides[gene].keys())-1):
        for t2 in range((t+1), len(peptides[gene].keys())):
            transcript = list(peptides[gene].keys())[t]
            transcript2 = list(peptides[gene].keys())[t2]
            r=peptides[gene][transcript].difference(peptides[gene][transcript2])
            r2=peptides[gene][transcript2].difference(peptides[gene][transcript])
            res.append([gene, transcript, transcript2, r, r2])
print(len(res))


# Step 4: check for each of the peptides in the peptides lists that dicriminate
#  the two transcripts wheter it is associated to more than one gene.
# Use the search tree that has been constructed earlier, to quickly find
# the number of genes associated to each peptide and check wheter there is
# only one.
trAndGene= []
for item in res:
        r=[]
        r2=[]
        for pep in item[3]:
                if len(list(A.get(pep).keys()))==1:
                        r.append(pep)
        for pep in item[4]:
                if len(list(A.get(pep).keys()))==1:
                        r2.append(pep)
        if r!=[] or r2!=[]:
                trAndGene.append([item[0], item[1], item[2], r, r2])

print(len(trAndGene))
# Write the lists to a file:
# Format gene, transcript which contains pepide, transcript which it discrimates from
# peptide (length ranges from args.min_pep to args.max_pep)
def writer(res, filename, filename2):
        with open (filename, "w") as handle, open(filename2, "w") as long_handle:
                for item in res:
                        for pep in item[3]:
                                if len(pep)<args.max_pep and len(pep)>=args.min_pep:
                                        handle.write(item[0]+"\t"+item[1]+"\t"+item[2]+"\t"+pep+"\n")
                                else:
                                        long_handle.write(item[0]+"\t"+item[1]+"\t"+item[2]+"\t"+pep+"\n")
                        for pep in item[4]:
                                if len(pep)<args.max_pep and len(pep)>=args.min_pep:
                                        handle.write(item[0]+"\t"+item[2]+"\t"+item[1]+"\t"+pep+"\n")
                                else:
                                        long_handle.write(item[0]+"\t"+item[2]+"\t"+item[1]+"\t"+pep+"\n")


writer(res, os.path.join(args.outdir, "transcript_disc.tsv"), os.path.join(args.outdir,"transcript_disc_long.tsv"))
writer(trAndGene, os.path.join(args.outdir,"transcript_gene_disc.tsv"), os.path.join(args.outdir,"transcript_gene_disc_long.tsv"))
