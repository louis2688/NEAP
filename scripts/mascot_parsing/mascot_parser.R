setwd("/home/nick/neap/git/NEAP")
library(pepXMLTab)

peps <- pepXML2tab("/media/nick/Volume/neap/project/other_data/mascot/Adult_Adrenalgland_Gel_Elite_49.pep.xml")  #too big, full RAM
peps <- pepXML2tab("/media/nick/Volume/neap/project/other_data/mascot/Adult_Pancreas_Gel_Elite_60.pep.xml")

peps_short<-peps[,c("peptide","protein","IonScore")]
write.table(peps_short,file = "/media/nick/Volume/neap/project/other_data/mascot/Adult_Pancreas_Gel_Elite_60.csv", sep = "\t",quote = F,row.names = F)
