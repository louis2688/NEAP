from Bio import SeqIO
import ahocorasick
import os.path
import argparse
parser = argparse.ArgumentParser(description='Read Coverage')
parser.add_argument('inputfile', metavar="Input", type=str,
                                        help='evidence file')
parser.add_argument('outdir', metavar="Input", type=str,
                                        help='outdir file')
parser.add_argument('protfile', metavar="Input", type=str,
                                        help='proteome file')

args = parser.parse_args()
outdir=args.outdir

A = ahocorasick.Automaton()
silico={}
ppg={}
with open(args.inputfile, "r") as handle:
    for line in handle:
        l=line.rstrip().split("\t")
        for pep in l[2].split("|"):
            if pep not in A:
                A.add_word(pep, 1)
            else:
				A.get(pep)=A.get(pep)+1

          

A.make_automaton()

