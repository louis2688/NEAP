library(shiny)
library(plotly)
library(data.table)
require(stringr)
library("scales")
library("ggplot2", lib.loc="~/R/x86_64-pc-linux-gnu-library/3.2")

function(input, output, session) {
    options(shiny.maxRequestSize=500*1024^2)
    output$plot <- renderPlotly({
        
        # input$file1 will be NULL initially. After the user selects
        # and uploads a file, it will be a data frame with 'name',
        # 'size', 'type', and 'datapath' columns. The 'datapath'
        # column will contain the local filenames where the data can
        # be found.
        
        inFile <- input$file1
        # 
        if (is.null(inFile))
            return(NULL)
        
        #inFile<-input$file1[[1, 'datapath']]
        dat_ev<-read.table(input$file1[[1, 'datapath']], header=input$header, sep=input$sep,fill = T,stringsAsFactors = F)
        dat_is<-read.table(input$file1[[2, 'datapath']], header=input$header, sep=input$sep,fill = T,stringsAsFactors = F)
        #    dat_is<-read.table("./data/kim/adrenal.gland_evidence.txt", header=T, sep="\t",fill = T,stringsAsFactors = F)
        #    dat_ev<-read.table("./data/kim/colon_evidence.txt", header=T, sep="\t",fill = T,stringsAsFactors = F)
        dat_ev$nchar<-nchar(dat_ev[,input$num])
        dat_is$nchar<-nchar(dat_is[,input$num])
        #    dat_ev$nchar<-nchar(dat_ev[,1])
        #   dat_is$nchar<-nchar(dat_is[,1])
        
        p <- 
            ggplot()+ 
            geom_bar(aes(dat_is$nchar),color=NA,fill="steelblue3")+
            #   geom_point(aes(x= ,y=dat_is$nchar), pch=15, fill="steelblue3", size=4)+
            geom_vline(xintercept =  min(dat_is$nchar),linetype = "dashed", colour = "red")+
            geom_vline(xintercept =  max(dat_is$nchar),linetype = "dashed", colour = "red")+
            geom_bar(aes(dat_ev$nchar),color=1,fill=NA)+
            #    scale_y_continuous(trans = 'log10',breaks = trans_breaks('log10', function(x) 10^x), labels = trans_format('log10', math_format(10^.x)))+
            #    scale_y_log10(breaks=(c(1,10,50,100,500,1000,5000,10000,50000,100000,1000000)))+
            scale_x_continuous(expand = c(0,0),limits = c(0,58), breaks = scales::pretty_breaks(n = 22)) +
            theme(axis.text.x = element_text(angle = -45, hjust = 0.1)) +
            ggtitle(print(paste0("Peptide length distribution of\n\"", input$file1[[1, 'name']],"\" vs. \"",input$file1[[2, 'name']],"\""))) +
            theme(plot.title = element_text(hjust = 0.5))+
            ylab("Number of peptides")+
            xlab("Peptide length")
        #   annotate("text",x=2,y=max(dat_is$nchar),label=n_ev)
        ggplotly(p)
        
        #ggplot(data=table_df, aes(x=pl1,y=c1,group=origin))+geom_line()
        # geom_line(data=table_df_u,aes(color="Second line"))+
        #  labs(color="Legend text")
        
    })
    output$plot2 <- renderPlotly({
        
        inFile <- input$file1
        # 
        if (is.null(inFile))
            return(NULL)
        
        #inFile<-input$file1[[1, 'datapath']]
        dat_ev<-read.table(input$file1[[1, 'datapath']], header=input$header, sep=input$sep,fill = T,stringsAsFactors = F)
        dat_is<-read.table(input$file1[[2, 'datapath']], header=input$header, sep=input$sep,fill = T,stringsAsFactors = F)
        #    dat_is<-read.table("./data/kim/adrenal.gland_evidence.txt", header=T, sep="\t",fill = T,stringsAsFactors = F)
        #    dat_ev<-read.table("./data/kim/colon_evidence.txt", header=T, sep="\t",fill = T,stringsAsFactors = F)
        dat_ev$nchar<-nchar(dat_ev[,input$num])
        dat_is$nchar<-nchar(dat_is[,input$num])
        
        #cumsum plot
        table_df<-as.data.frame(table(dat_ev$nchar))
        table_df$perc<-table_df[,2]/sum(table_df$Freq)
        table_df$cumsum<-cumsum(table_df$perc)
        table_df$Peptides<-print(paste0(input$file1[[1, 'name']],""))
        # table_df$Peptides<-"ev"
        colnames(table_df)<-c("pl1","f1","p1","c1","origin")
        # table_df$pl1<-as.character(table_df$pl1)
        table_df$pl1 = as.double(levels(table_df$pl1))[table_df$pl1] # <-- converting 
        
        table_df_u<-as.data.frame(table(dat_is$nchar))
        table_df_u$perc<-table_df_u[,2]/sum(table_df_u$Freq)
        table_df_u$cumsum<-cumsum(table_df_u$perc)
        table_df_u$d<-print(paste0(input$file1[[2, 'name']],""))
        # table_df_u$Peptides<-"is"
        colnames(table_df_u)<-c("pl2","f2","p2","c2","origin")
        table_df_u$pl2 = as.double(levels(table_df_u$pl2))[table_df_u$pl2] # <-- converting 
        # table_df_u$pl2<-as.character(table_df_u$pl2)
        #table_df_combined<-cbind(table_df,table_df_u)
        
        q <- ggplot()+ 
            geom_step(data = table_df, aes(x=pl1, y=c1,group=origin,color=origin))+
            geom_step(data = table_df_u, aes(x=pl2, y=c2,group=origin,color=origin))+
            #   expand_limits(x=0,y=0)+
            #   scale_x_discrete(breaks=seq(1,60,1), labels=seq(1,60,1))+
            theme(axis.text.x = element_text(angle = -45, hjust = 0.1)) +
            ggtitle(print(paste0("Cumulative percentage of peptide length:\n\"", input$file1[[1, 'name']],"\" vs. \"",input$file1[[2, 'name']],"\""))) +
            theme(plot.title = element_text(hjust = 0.5))+
            ylab("Cumulative percentage")+
            xlab("Peptide length")
        ggplotly(q)
    })
    output$text1 <- renderText({
        
        inFile <- input$file1
        # 
        if (is.null(inFile))
            return(NULL)
        
        #inFile<-input$file1[[1, 'datapath']]
        dat_ev<-read.table(input$file1[[1, 'datapath']], header=input$header, sep=input$sep,fill = T,stringsAsFactors = F)
        dat_is<-read.table(input$file1[[2, 'datapath']], header=input$header, sep=input$sep,fill = T,stringsAsFactors = F)
        #    dat_is<-read.table("./data/kim/adrenal.gland_evidence.txt", header=T, sep="\t",fill = T,stringsAsFactors = F)
        #    dat_ev<-read.table("./data/kim/colon_evidence.txt", header=T, sep="\t",fill = T,stringsAsFactors = F)
        dat_ev$nchar<-nchar(dat_ev[,input$num])
        dat_is$nchar<-nchar(dat_is[,input$num])
        n_ev<-length(unique(dat_ev[,input$num]))
        n_is<-length(unique(dat_is[,input$num]))
        
        output$text1 <- renderText({ 
            print(paste0(input$file1[[1, 'name']],": ", n_ev,"        ",input$file1[[2, 'name']],": ", n_is))
            #     print(paste0(input$file1[[2, 'name']],"", n_is))
        })
        
    })
    output$coveragePlot<-renderPlot({
        print("Doing stuff")
        source("~/NEAP/NEAP/scripts/coverage/color_scheme.R")
        coverage<-"~/NEAP/NEAP/results/03_coverage_plots/data/kim/adrenal.gland_evidence/adrenal.gland_evidence_coverage.tsv"
        proteome<-"~/NEAP/NEAP/data/human_proteome/Homo_sapiens.GRCh38.pep.all.fa"
        proteome_ranges<-"~/NEAP/NEAP/data/human_proteome/petide_ranges.tsv"
        d_count<-fread(coverage, header=F, sep="\t")
        proteome<-read.fasta(proteome, seqtype = "AA", as.string = T)
        peptide_ranges<-fread(proteome_ranges, header=T, sep="\t")
        
        
        co <-colnames(d_count)
        d_count$V3<-d_count$V3+1
        d_count$V4<-d_count$V4+1
        
        fun<-function(x){
            c(
                str_sub(attr(x, "Annot"),str_locate(attr(x, "Annot"), "gene[0-9A-Z:.]*")), 
                str_sub(attr(x, "Annot"),str_locate(attr(x, "Annot"), "transcript[0-9A-Z:.]*")), 
                x[1]
            )
        }
        
        seqs<-lapply(proteome, function(x) fun(x))
        seqs<-do.call(rbind, seqs)
        seqs<-as.data.frame(seqs)
        prot_seqs<-AAStringSet(seqs$V3)
        names(prot_seqs)<-sapply(seqs$V2, function(x) gsub("transcript:", "", x))
        names(prot_seqs)<-sapply(names(prot_seqs), function(x) gsub("\\.[0-9]*", "", x, perl=T))
        t<-unique(d_count$V1)
        prot_seqs<-prot_seqs[t]
        
        
        
        
        
        pep_wrapper<-function(txname, peptide_ranges){
            pep<-peptide_ranges[transcript==txname]
            pep[, width:=(V2-V1)/3]
            pep
        }
        shifter<-function(x, probe){
            idx<-which(probe$V3>x[[1]])
            probe$V3[idx]<-probe$V3[idx]+(x[[2]]-x[[1]])+1
            probe$V4[idx]<-probe$V4[idx]+(x[[2]]-x[[1]])+1
            probe
        }
        shift_wrapper<-function(shift, probe){
            for(i in 1:nrow(shift)){
                probe<-shifter(shift[i,], probe)
            }
            probe
        }
        probe_merger<-function(probes1, probes2){
            p<-merge(probes1[,1:4], probes2[,1:4], by=c("V2", "V3", "V4"), all=T, allow.cartesian=T)
            p[, intens:=rep(1,nrow(p))]
            p$intens[which(is.na(p$V1.x))]<-2
            p$intens[which(is.na(p$V1.y))]<-3
            p
        }
        boundary<-function(pep, shift){
            ir<-IRanges(pep$V1, pep$V2)
            boundaries<-ir@width/3
            boundaries<-sapply(1:length(boundaries), function(x) sum(boundaries[1:x]))
            if(nrow(shift)!=0){
                for (s in 1:nrow(shift)){
                    boundaries[boundaries>as.integer(shift[s,1])]<-boundaries[boundaries>as.integer(shift[s,1])]+as.integer(shift[s,2])-as.integer(shift[s,1])+1
                }
            }
            boundaries<-c(1, boundaries)
            round(boundaries, digits=0)
        }
        seq_wrapper<-function(seq, boundaries){
            seq<-ProteinSequenceTrack(seq)
            ht<-HighlightTrack(trackList=seq, start=boundaries, width=0)
            ht
        }
        
        tx1_name="ENST00000396858"
        tx2_name="ENST00000229239"
        pep1<-pep_wrapper(tx1_name, peptide_ranges)
        pep2<-pep_wrapper(tx2_name, peptide_ranges)
        tx1<-prot_seqs[tx1_name]
        tx2<-prot_seqs[tx2_name]
        ali<-pairwiseAlignment(tx1,tx2)
        probes2<-d_count[V1==tx2_name]
        probes1<-d_count[V1==tx1_name]
        shift1<-as.data.table(do.call(rbind,str_locate_all(as.character(pattern(ali)), "-+")))
        shift2<-as.data.table(do.call(rbind,str_locate_all(as.character(subject(ali)), "-+")))
        probes1<-shift_wrapper(shift1, probes1)
        probes2<-shift_wrapper(shift2, probes2)
        p<-probe_merger(probes1,probes2)
        boundaries1<-boundary(pep1, shift1)
        boundaries2<-boundary(pep2, shift2)
        lili<-list()
        lili["seq1"]<-seq_wrapper(pattern(ali), boundaries1)
        lili["seq2"]<-seq_wrapper(subject(ali), boundaries2)
        lili["probe1"]<-ProbeTrack(sequence=p$V2, probeStart=p$V3, intensity=p$intens, , cex=0, color=c("#ff9900", "#990000", "#006600"))
        lili["axisTrack"]<-ProteinAxisTrack()
        plotTracks(lili,fontcolor=fcol)
    })
}