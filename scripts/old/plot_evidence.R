setwd("/home/nick/neap/git/NEAP")
library("ggplot2", lib.loc="~/R/x86_64-pc-linux-gnu-library/3.2")
library("Peptides")
library("scales")
library(data.table)
#source("http://bioconductor.org/biocLite.R")
#biocLite("Biostrings")
#require(Biostrings)

folder_wilhelm <- "/home/nick/neap/git/NEAP/data/wilhelm/"   # path to folder that holds multiple evidence files
file_list_wilhelm <- list.files(path=folder_wilhelm, pattern="*.txt") # create list of all evidence files in folder

folder_kim <- "/home/nick/neap/git/NEAP/data/kim/"   # path to folder that holds multiple evidence files
file_list_kim <- list.files(path=folder_kim, pattern="*.txt") # create list of all evidence files in folder

# read in each evidence file in file_list and rbind them into a data frame called data 
data_kim <- 
  do.call("rbind", 
          lapply(file_list_kim, 
                 function(x) 
                   read.csv(paste(folder_kim, x, sep=''), 
                            sep = "\t",header=T,fill=T, row.names = NULL)))

data_wilhelm <- 
  do.call("rbind", 
          lapply(file_list_wilhelm, 
                 function(x) 
                   read.csv(paste(folder_wilhelm, x, sep=''), 
                            sep = "\t",header=T,fill=T, row.names = NULL)))

write.table(data_kim, "./results/04_evidence_peptide_length/kim_combined_evidence.txt", sep="\t")
write.table(data_wilhelm, "./results/04_evidence_peptide_length/wilhelm_combined_evidence.txt", sep="\t")


header<-as.vector(read.table("./data/header.txt",sep = "\t",stringsAsFactors = F))  #header
pan2<-read.table("./data/kim/pancreas_mapping_missed.txt", sep = "\t",fill = T,col.names = header) #in_silico to orig_pancreas mapped (.gz)
pan_orig<-read.table("/media/nick/Volume/neap/project/kim/pancreas_evidence.txt",sep = "\t",fill = T,header = T) #original pancreas evidence peptides

pan2<-data
#peptide length
pan2$Sequence<-as.character(pan2$Sequence)
pan2$nchar<-nchar(pan2$Sequence)
u_pan2<-as.data.frame(unique(pan2$Sequence))
colnames(u_pan2)<-"Sequence"
u_pan2$Sequence<-as.character(u_pan2$Sequence)
u_pan2$nchar<-nchar(u_pan2$Sequence)

#unique peptides
pan2$Sequence<-factor(pan2$Sequence)
nlevels(pan2$Sequence)

#peptide origin
table(pan2$Peptide_transcript_origin)

#whole plot:
ggplot()+ 
  geom_histogram(aes(pan2$nchar),binwidth = 1,color=1,fill="steelblue2")+
#  geom_histogram(aes(u_pan2$nchar),binwidth = 1,color=1,fill="steelblue4")+
  scale_y_continuous(trans = 'log10',breaks = trans_breaks('log10', function(x) 10^x), labels = trans_format('log10', math_format(10^.x)))+
#  scale_y_log10()+
  scale_x_continuous(expand = c(0,0),limits = c(0,58), breaks = scales::pretty_breaks(n = 22)) +
  theme(axis.text.x = element_text(angle = -45, hjust = 0.1)) +
  ggtitle("Peptide length distribution of all evidences:\nWilhelm et al.") +
  theme(plot.title = element_text(hjust = 0.5))+
  ylab("Number of peptides")+
  xlab("Peptide length")

#cumsum plot
table_df<-as.data.frame(table(pan2$nchar))
table_df$perc<-table_df[,2]/sum(table_df$Freq)
table_df$cumsum<-cumsum(table_df$perc)
table_df$Peptides<-"All peptides"
colnames(table_df)<-c("pl1","f1","p1","c1")
table_df_u<-as.data.frame(table(u_pan2$nchar))
table_df_u$perc<-table_df_u[,2]/sum(table_df_u$Freq)
table_df_u$cumsum<-cumsum(table_df_u$perc)
table_df_u$d<-"Unique peptides"
colnames(table_df_u)<-c("pl2","f2","p2","c2")
table_df_combined<-cbind(table_df,table_df_u)

ggplot(table_df_combined)+ 
#  aes(x=1:length(table_df$Var1), y=table_df$cumsum)+
  geom_line(aes(x=pl1, y=c1,group=Peptides,color=Peptides))+
  geom_line(aes(x=pl1, y=c2,group=1,color=d))+
  geom
  theme(axis.text.x = element_text(angle = -45, hjust = 0.1)) +
  ggtitle("Cumulative percentage of pancreas-mapped peptides") +
  theme(plot.title = element_text(hjust = 0.5))+
  ylab("Cumulative percentage")+
  xlab("Peptide length")

# aa compositon
us<-unique(pan_orig$Sequence)
map_ac<-aaComp(us)
map_ac_df<-t(as.data.frame(map_ac))
grep<-grepl("^Mole", rownames(map_ac_df))
map_ac_df<-map_ac_df[grep,]
map_ac_df_melt<- melt(map_ac_df)
colnames(map_ac_df_melt)<-c("Mol","Characteristic","Percentage")

#plot aa comp
ggplot(data=map_ac_df_melt)+ 
  aes(x=map_ac_df_melt$Characteristic,y=map_ac_df_melt$Percentage,fill=map_ac_df_melt$Characteristic)+
  geom_boxplot(outlier.shape = NA)+
  theme(axis.text.x = element_text(angle = -45, hjust = -0.1,size=11)) +
  theme(axis.text.y = element_text(size=11), text = element_text(size=12))+
  ggtitle("Pancreas peptides:\nAmino acid composition") +
  theme(plot.title = element_text(hjust = 0.5,size=14))+
  guides(fill=F)+
  ylab("Occurences in %")+
  xlab("")
