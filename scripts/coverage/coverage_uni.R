require(data.table)
require(optparse)

option_list <- list(
  make_option(c("-e", "--evidence"), type="character",
              help="Evidence file"),
  make_option(c("-o", "--outfile"), type="character",
              help="Peptide file")
)
opt <- parse_args(OptionParser(option_list=option_list))

cov<-fread(opt$evidence, sep="\t", header=F)
colnames(cov)<-c("gene", "tx", "start", "stop", "pep", "NA")
cov<-cov[,1:5]
cov$start<-cov$start+1
cov$stop<-cov$stop+1

cov_tab<-cov[ , gene,by=c("pep")]
cov_tab<-unique(cov_tab)
cov_tab<-cov_tab[, .N, by=c("pep")]
not_unique<-cov_tab[cov_tab$N!=1]

cov<-cov[!(pep %in% not_unique$pep),]

fwrite(cov, opt$outfile, sep="\t", quote = F)
