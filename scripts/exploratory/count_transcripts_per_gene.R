require(data.table)
require(optparse)
require(ggplot2)
require(seqinr)
require(stringr)

option_list <- list(
  make_option(c("-p", "--proteome"), type="character",
              help="Proteome file"),
  make_option(c("-o", "--outdir"), type="character",
              help="outdir")
)
opt <- parse_args(OptionParser(option_list=option_list))
seqs<-fread(opt$proteome, sep="\t", header=T)
count<-seqs[, .N, by=c("gene")]
fwrite(count, file.path(opt$outdir, "transcripts_per_gene.tsv"), sep="\t")

pdf(file.path(opt$outdir, "transcripts_per_gene_hist.pdf"))
transcripts_per_gene_hist<-ggplot(count, aes(N))+geom_histogram(bins=75)+xlab("#Transcripts")+ylab("#Genes")+ggtitle("Number of transcripts per gene")
plot(transcripts_per_gene_hist)
dev.off()
png(file.path(opt$outdir, "transcripts_per_gene_hist_0_25.png"), width=600, height = 600)
transcripts_per_gene_hist_zoomed<-ggplot(count, aes(N))+geom_histogram(bins=25)+xlab("#Transcripts")+ylab("#Genes")+
    ggtitle("Number of transcripts per gene")+xlim(0,25)
plot(transcripts_per_gene_hist_zoomed)
dev.off()


